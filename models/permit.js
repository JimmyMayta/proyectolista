const { Schema, model } = require("mongoose");

const PermitSchema = new Schema({
  permit: {
    type: String,
    lowercase: true,
    trim: true,
    unique: true,
    default: null,
    required: [true, "Permit"],
  },
  permitname: {
    type: String,
    lowercase: true,
    trim: true,
    unique: true,
    default: null,
    required: [true, "Permit Name"],
  },
  state: {
    type: String,
    lowercase: true,
    trim: true,
    unique: false,
    default: "active",
    emun: ["active", "inactive", "deleted"],
    required: [false, "State"],
  },
  comment: {
    type: String,
    lowercase: true,
    trim: true,
    unique: false,
    default: null,
    required: [false, "Comment"],
  },
  description: {
    type: String,
    lowercase: true,
    trim: true,
    unique: false,
    default: null,
    required: [false, "Description"],
  },
  detail: {
    type: String,
    lowercase: true,
    trim: true,
    unique: false,
    default: null,
    required: [false, "Detail"],
  },
  creationdate: {
    type: String,
    lowercase: true,
    trim: true,
    unique: false,
    default: null,
    required: [false, "Creation Date"],
  },
  updatedate: {
    type: String,
    lowercase: true,
    trim: true,
    unique: false,
    default: null,
    required: [false, "Update Date"],
  },
  deletedate: {
    type: String,
    lowercase: true,
    trim: true,
    unique: false,
    default: null,
    required: [false, "Delete Date"],
  },
}, {collection:'permit'});

module.exports = model("Permit", PermitSchema);
