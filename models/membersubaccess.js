const { Schema, model } = require("mongoose");

const MemberSubAccessSchema = new Schema(
  {
    member: {
      type: Schema.Types.ObjectId,
      ref: "Member",
      lowercase: false,
      trim: false,
      unique: false,
      default: null,
      required: [false, "Member"],
    },
    access: {
      type: Schema.Types.ObjectId,
      ref: "Access",
      lowercase: false,
      trim: false,
      unique: false,
      default: null,
      required: [false, "Access"],
    },
    state: {
      type: String,
      lowercase: true,
      trim: true,
      unique: false,
      default: "active",
      emun: ["active", "inactive", "deleted"],
      required: [false, "State"],
    },
    comment: {
      type: String,
      lowercase: true,
      trim: true,
      unique: false,
      default: null,
      required: [false, "Comment"],
    },
    description: {
      type: String,
      lowercase: true,
      trim: true,
      unique: false,
      default: null,
      required: [false, "Description"],
    },
    detail: {
      type: String,
      lowercase: true,
      trim: true,
      unique: false,
      default: null,
      required: [false, "Detail"],
    },
    creationdate: {
      type: String,
      lowercase: true,
      trim: true,
      unique: false,
      default: null,
      required: [false, "Creation Date"],
    },
    updatedate: {
      type: String,
      lowercase: true,
      trim: true,
      unique: false,
      default: null,
      required: [false, "Update Date"],
    },
    deletedate: {
      type: String,
      lowercase: true,
      trim: true,
      unique: false,
      default: null,
      required: [false, "Delete Date"],
    },
  },
  { collection: "membersubaccess" }
);

module.exports = model("MemberSubAccess", MemberSubAccessSchema);
