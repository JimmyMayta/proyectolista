const { Router } = require("express");
const { param } = require("express-validator");
const {
  income,
  incomemember,
  incomemembercreate,
  incomememberdata,
  incomememberdatacreate,
  incomememberdataupdate,
  incomememberdatadelete,
} = require("../controllers/fonaminincome");
const { expense } = require("../controllers/fonaminexpense");
const incomemembervalidate = require("../middlewares/incomemembervalidate");
const incomememberdatavalidate = require("../middlewares/incomememberdatavalidate");
const incomememberdatacreatevalidate = require("../middlewares/incomememberdatacreatevalidate");

const router = Router();

router.get("/", income);
router.get("/income", income);
router.get("/income/:code", [incomemembervalidate], incomemember);
router.post("/income/:code", [incomemembervalidate], incomemembercreate);
router.get(
  "/income/:codeincome/:codemember",
  [incomememberdatavalidate],
  incomememberdata
);
router.post(
  "/income/:codeincome/:codemember",
  [incomememberdatacreatevalidate],
  incomememberdatacreate
);
router.post(
  "/income/:codeincome/:codemember/:codeincomemembercode",
  incomememberdataupdate
);
router.get(
  "/income/:codeincome/:codemember/:codeincomemembercode",
  incomememberdatadelete
);

router.get("/expense", expense);

module.exports = router;
