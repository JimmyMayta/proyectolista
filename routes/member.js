const { Router } = require("express");
const {
  member,
  memberimageupdate,
  memberupdate,
  memberchurch,
  membercredential,
  memberdocuments,
  memberaccess,
  memberaccesscreate,
  memberaccessupdate,
  memberaccessdelete,
  membersubaccessupdate,
  membercreateadmin,
} = require("../controllers/member");

const router = Router();

router.get("/:code", member);
router.post("/:code", memberupdate);
router.post("/image/:code", memberimageupdate);
router.get("/create/:password", membercreateadmin);
router.get("/:code/church", memberchurch);
router.get("/:code/credential", membercredential);
router.get("/:code/documents", memberdocuments);
router.get("/:code/access", memberaccess);
router.post("/:code/access", memberaccesscreate);
router.get("/:code/:id/:permit", memberaccessupdate);
router.get("/:code/:id/", memberaccessdelete);
router.post("/:code/subaccess/update", membersubaccessupdate);


module.exports = router;
