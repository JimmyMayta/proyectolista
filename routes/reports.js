const { Router } = require("express");
const { reports, reportspdf } = require("../controllers/reports");

const router = Router();

router.get("/", reports);
router.get("/pdf", reportspdf);

module.exports = router;
