const { Router } = require("express");
const { national } = require("../controllers/national");

const router = Router();

router.get("/", national);

module.exports = router;
