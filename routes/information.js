const { Router } = require("express");
const { information } = require("../controllers/information");

const router = Router();

router.get("/", information);

module.exports = router;
