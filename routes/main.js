const { Router } = require("express");
const { main,  activities, nosotros, contact } = require("../controllers/main");

const router = Router();

router.get("/", main);
router.get("/activities", activities);
router.get("/nosotros", nosotros);
router.get("/contact", contact);

module.exports = router;
