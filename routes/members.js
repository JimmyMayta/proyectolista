const { Router } = require("express");
const {
  members,
  memberspdf,
  membersxlsx,
  memberscreate,
} = require("../controllers/members");

const router = Router();

router.get("/", members);
router.get("/pdf", memberspdf);
router.get("/xlsx", membersxlsx);
router.post("/", memberscreate);
router.get("/:search", members);

module.exports = router;
