const { Router } = require("express");
const { country, countrycreate, countrystate } = require("../controllers/country");

const router = Router();

router.get("/", country);
router.post("/", countrycreate);
router.get("/:id/:state", countrystate);

module.exports = router;
