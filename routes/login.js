const { Router } = require("express");
const { login, logout } = require("../controllers/login");

const router = Router();

router.post("/", login);
router.get("/", logout);

module.exports = router;
