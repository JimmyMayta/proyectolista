const { Router } = require("express");
const { district,districtcreate, districtcountrycreate, districtdepartmentcreate } = require("../controllers/district");

const router = Router();

router.get("/", district);
router.post("/", districtcreate);
router.post("/country", districtcountrycreate);
router.post("/department", districtdepartmentcreate);

module.exports = router;
