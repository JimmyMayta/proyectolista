const { Router } = require("express");
const {
  controlmain,
  controlmaincreate,
  controlmainupdate,
  controlmaindelete,
  controlmainactivitiesupdate,
  controlactivities,
  controlnosotros,
  controlcontact,
  controlfonamin,
  controlfonaminincomecreate,
  controlfonaminincomeupdate,
  controlfonaminincomedelete,
  controlfonaminincomestates,
  controlfonaminexpensecreate,
  controlfonaminexpenseupdate,
  controlfonaminexpensedelete,
  controlfonaminexpensestates,
} = require("../controllers/maincontrol");

const router = Router();

router.get("/", controlmain);
router.get("/main", controlmain);
router.post("/main", controlmaincreate);
router.post("/main/update", controlmainupdate);
router.post("/mainactivities/update", controlmainactivitiesupdate);
router.get("/main/delete/:id", controlmaindelete);
router.get("/activities", controlactivities);
router.get("/nosotros", controlnosotros);
router.get("/contact", controlcontact);
router.get("/fonamin", controlfonamin);
router.post("/fonamin/income/create", controlfonaminincomecreate);
router.post("/fonamin/income/update/:id", controlfonaminincomeupdate);
router.get("/fonamin/income/delete/:id", controlfonaminincomedelete);
router.get("/fonamin/income/states/:id", controlfonaminincomestates);
router.post("/fonamin/expense/create", controlfonaminexpensecreate);
router.post("/fonamin/expense/update/:id", controlfonaminexpenseupdate);
router.get("/fonamin/expense/delete/:id", controlfonaminexpensedelete);
router.get("/fonamin/expense/states/:id", controlfonaminexpensestates);

module.exports = router;
