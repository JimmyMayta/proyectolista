const { Router } = require("express");
const { churches, churchescreate } = require("../controllers/churches");

const router = Router();

router.get("/", churches);
router.post("/", churchescreate);

module.exports = router;
