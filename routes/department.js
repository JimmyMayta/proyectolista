const { Router } = require("express");
const { department, departmentcreate } = require("../controllers/department");

const router = Router();

router.get("/", department);
router.post("/", departmentcreate);

module.exports = router;
