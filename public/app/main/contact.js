
const latitude = -16.4911
const longitude = -68.1492
const zoom = 16

var map = L.map('map').setView([latitude, longitude], zoom);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

L.marker([latitude, longitude]).addTo(map)
    .bindPopup('IEBS Nacional')
    .openPopup();

document.querySelector('#map').children[1].children[3].children[0].textContent = ''
