const members = JSON.parse(document.querySelector("#members").value).map(
  (item) => {
    return {
      code: item.code,
      fullname: `${item.names} ${item.firstlastname} ${item.secondlastname}`,
      identitynumber: item.identitynumber,
    };
  }
);
const member = document.querySelector("#member");
const memberlist = document.querySelector("#memberlist");
const membercode = document.querySelector("#membercode");

member.addEventListener("keyup", (event) => {
  memberlist.textContent = "";

  if (member.value.length >= 3) {
    const result = members.filter((item) =>
      item.fullname.includes(member.value.trim())
    );

    result.slice(-3).forEach((item) => {
      let li = document.createElement("li");
      li.className = "list-group-item";
      li.textContent = item.fullname;
      li.setAttribute("membercode", item.code);
      memberlist.appendChild(li);
    });
  }
});

memberlist.addEventListener("click", (event) => {
  member.value = event.target.innerText;
  membercode.value = event.target.getAttribute('membercode');
  memberlist.textContent = "";
});
