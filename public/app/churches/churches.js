$(document).ready(function () {
  $("#table").DataTable({
    responsive: true,
    language: { url: "/lib/datatables/es-ES.json" },
    autoWidth: true,
  });
});

const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);

const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);
