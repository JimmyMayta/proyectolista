// $(document).ready(function () {
//   $("#table").DataTable({
//     responsive: true,
//     language: {
//       url: "/lib/datatables/es-ES.json",
//     },
//   });
// });

const searchinput = document.querySelector("#searchinput");
const search = document.querySelector("#search");

const tooltipTriggerList = document.querySelectorAll(
  '[data-bs-toggle="tooltip"]'
);

const tooltipList = [...tooltipTriggerList].map(
  (tooltipTriggerEl) => new bootstrap.Tooltip(tooltipTriggerEl)
);

searchinput.addEventListener("keyup", (e) => {
  if (searchinput.value === "") {
    window.location.href = `/members`;
  }
  if (e.key === "Enter") {
    window.location.href = `/members/${searchinput.value}`;
  }
  search.href = `/members/${searchinput.value}`;
});
