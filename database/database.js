const mongoose = require("mongoose");
const {
  production,
  mongodbpro,
  mongodb,
  database,
} = require("../environments/environment");

const DBConnection = async () => {
  try {
    const mongo = production ? mongodbpro : mongodb;
    mongoose.set("strictQuery", false);
    await mongoose.connect(`${mongo}/${database}`);
    console.log("Database Online");
  } catch (error) {
    console.log("Database Offline");
  }
};

module.exports = DBConnection;
