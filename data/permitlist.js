module.exports = [
  { permit: "create", permitname: "Crear" },
  { permit: "update", permitname: "Actualizar" },
  { permit: "delete", permitname: "Eliminar" },
];
