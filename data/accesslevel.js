module.exports = [
  {
    level: "country",
    levelname: "Pais",
  },
  {
    level: "national",
    levelname: "Nacional",
  },
  {
    level: "district",
    levelname: "Distrital",
  },
  {
    level: "churches",
    levelname: "Iglesia",
  },
  {
    level: "personal",
    levelname: "Personal",
  },
];
