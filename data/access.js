module.exports = [
  {
    access: "members",
    accessname: "Miembros",
  },
  {
    access: "membersmembers",
    accessname: "Miembros - Membros",
  },
  {
    access: "memberpersonal",
    accessname: "Miembros - Personal",
  },
  {
    access: "memberchurch",
    accessname: "Miembro - Iglesia",
  },
  {
    access: "memberdocuments",
    accessname: "Miembro - Documentos",
  },
  {
    access: "membercredential",
    accessname: "Miembro - Credencial",
  },
  {
    access: "memberaccess",
    accessname: "Miembro - Accesos",
  },

  {
    access: "churchchurch",
    accessname: "Iglesia",
  },
  {
    access: "churchdistrict",
    accessname: "Distrito",
  },
  {
    access: "churchdepartment",
    accessname: "Departamentos",
  },
  {
    access: "churchcountry",
    accessname: "Pais",
  },
  {
    access: "churchnational",
    accessname: "Nacional",
  },
  {
    access: "reports",
    accessname: "Informes",
  },
  {
    access: "reportsreports",
    accessname: "Informes - Informes",
  },
  {
    access: "memberaccessaccess",
    accessname: "Miembro - Accesos - Acceso",
  },
];
