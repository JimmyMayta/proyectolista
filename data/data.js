const AccessModel = require("../models/access");
const LevelModel = require("../models/level");
const access = require("./access");
const accesslevel = require("./accesslevel");
const date = require("../tools/date");

const data = () => {
  access.forEach(async (e, i) => {
    const res = await AccessModel.findOne({ access: e.access });

    if (res === null) {
      const acceso = new AccessModel({
        access: e.access,
        accessname: e.accessname,
        accessnumber: i + 1,
        creationdate: date(),
      });
      await acceso.save();
    }
  });

  accesslevel.forEach(async (e, i) => {
    const res = await LevelModel.findOne({ level: e.level });

    if (res === null) {
      const nivel = new LevelModel({
        level: e.level,
        levelname: e.levelname,
        levelnumber: i + 1,
        creationdate: date(),
      });
      await nivel.save();
    }
  });
};

module.exports = data;
