const IncomeModel = require("../models/income");
const MemberModel = require("../models/member");

const incomememberdatavalidate = async (req, res, next) => {
  const { codeincome, codemember } = req.params;
  const income = await IncomeModel.findOne({
    incomecode: codeincome,
    state: "active",
  });
  const member = await MemberModel.findOne({
    code: codemember,
    state: "active",
  });

  if (income === null) {
    return res.redirect("/fonamin/income");
  }

  if (member === null) {
    return res.redirect("/fonamin/income");
  }

  return next();
};

module.exports = incomememberdatavalidate;
