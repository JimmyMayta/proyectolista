const IncomeModel = require("../models/income");
const MemberModel = require("../models/member");

const incomememberdatacreatevalidate = async (req, res, next) => {
  const { codeincome, codemember } = req.params;
  const { amount } = req.body;

  const income = await IncomeModel.findOne({
    incomecode: codeincome,
    state: "active",
  });
  const member = await MemberModel.findOne({
    code: codemember,
    state: "active",
  });

  if (income === null) {
    return res.redirect("/fonamin/income");
  }

  if (member === null) {
    return res.redirect("/fonamin/income");
  }

  if (isNaN(amount)) {
    return res.redirect("/fonamin/income");
  }

  if (amount === "") {
    return res.redirect("/fonamin/income");
  }

  return next();
};

module.exports = incomememberdatacreatevalidate;
