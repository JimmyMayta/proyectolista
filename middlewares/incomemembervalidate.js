const IncomeModel = require("../models/income");

const incomemembervalidate = async (req, res, next) => {
  const { code } = req.params;
  const income = await IncomeModel.findOne({ incomecode: code });

  if (income === null) {
    return res.redirect("/fonamin/income");
  }

  return next();
};

module.exports = incomemembervalidate;
