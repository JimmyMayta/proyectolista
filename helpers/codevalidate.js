const codevalidate = (value, { req, res, next }) => {
  if (value !== req.params.code.length === 20) {
    return res.redirect("/")
  }

  return true;
};

module.exports = codevalidate
