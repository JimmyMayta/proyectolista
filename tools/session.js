const MemberModel = require("../models/member");

const session = async (req) => {
  try {
    const member = await MemberModel.findOne(
      { code: req.session.session },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    );
    return member.code;
  } catch (error) {
    req.session = null;

    return false;
  }
};

module.exports = session;
