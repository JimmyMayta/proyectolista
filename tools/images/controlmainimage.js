const path = require("path");
const sharp = require("sharp");
const code = require("../code");
const fs = require("fs");

const controlmainimage = async (file) => {
  const image = file;
  const codigo = code();

  const imagemain = path.join(
    __dirname,
    `../../public/images/main/${codigo}.png`
  );

  const { width, height } = await sharp(image.tempFilePath).metadata();

  if (width === height) {
    await sharp(image.tempFilePath)
      .resize(Math.round((70 / 100) * height) + height, height)
      .toFile(imagemain);
  }

  if (width > height) {
    await sharp(image.tempFilePath)
      .resize(Math.round((70 / 100) * width) + width, width)
      .toFile(imagemain);
  }

  if (width < height) {
    await sharp(image.tempFilePath)
      .resize(Math.round((70 / 100) * height) + height, height)
      .toFile(imagemain);
  }

  const {
    format: imageextension,
    width: imagewidth,
    height: imageheight,
  } = await sharp(imagemain).metadata();

  return {
    image: codigo,
    imagename: `${codigo}.png`,
    imageextension,
    imagewidth,
    imageheight,
  };
};

const controlmainimagedelete = async (image) => {
  const imagemain = path.join(
    __dirname,
    `../../public/images/main/${image}.png`
  );

  if (fs.existsSync(imagemain)) {
    fs.unlinkSync(imagemain);
  }
};

module.exports = {controlmainimage, controlmainimagedelete};
