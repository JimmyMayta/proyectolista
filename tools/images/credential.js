const path = require("path");
const sharp = require("sharp");

const imagecredential = async (code) => {
  const input = path.join(__dirname, "../../images/credential/white.png");
  const output = path.join(
    __dirname,
    `../../public/images/credential/${code}.png`
  );
  const metadata = await sharp(input).metadata();

  await sharp({
    text: {
      text: "Hello, world!",
      width: 500,
      height: 100,

    },
  }).toFile(output);

  // await sharp({
  //   text: {
  //     text: '<span foreground="red">Red!</span><span background="cyan">blue</span>',
  //     font: "sans",
  //     rgba: true,
  //     dpi: 300,
  //   },
  // }).toFile(output);

  return metadata;
};

const imagecredentialback = async () => {
  const input = path.join(__dirname, "../../images/credential/white.png");
  const output = path.join(
    __dirname,
    "../../public/images/credential/white.png"
  );
  const metadata = await sharp(input).metadata();

  await sharp(input)
    .resize(500, Math.round((30 / 100) * 500) + 500)
    .toFile(output);

  return metadata;
};

module.exports = { imagecredential, imagecredentialback };
