const path = require("path");
const fs = require("fs");
const sharp = require("sharp");
const code = require("../code");

const imageupload = async (membercode, file) => {
  const { image } = file;

  const imagemember = path.join(
    __dirname,
    `../../public/images/member/${membercode}.png`
  );

  if (fs.existsSync(imagemember)) {
    fs.unlinkSync(imagemember);
  }

  const { width, height } = await sharp(image.tempFilePath).metadata();

  if (width === height) {
    await sharp(image.tempFilePath)
      .resize(width, Math.round((30 / 100) * width) + width)
      .toFile(imagemember);
  }

  if (width > height) {
    await sharp(image.tempFilePath)
      .resize(height, Math.round((30 / 100) * height) + height)
      .toFile(imagemember);
  }

  if (width < height) {
    await sharp(image.tempFilePath)
      .resize(width, Math.round((30 / 100) * width) + width)
      .toFile(imagemember);
  }

  const {
    format: imageextension,
    width: imagewidth,
    height: imageheight,
  } = await sharp(imagemember).metadata();

  return {
    image: membercode,
    imagename: `${membercode}.png`,
    imageextension,
    imagewidth,
    imageheight,
  };
};

module.exports = imageupload;
