const sum = async (numbers) => {
  let total = 0;
  numbers.forEach(num => total += Number(num));
  return total
};

module.exports = { sum };
