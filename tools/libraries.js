const moment = require("moment-timezone");
const Chance = require("chance");
const { v4: uuidv4 } = require("uuid");

const generatecode = () => {
  const chance = new Chance();

  return `${moment()
    .tz("America/La_Paz")
    .format("YYYYMMDDHHmmssSSS")}${chance.string({
    length: 3,
    pool: "123456789",
  })}`;
};

const generatecodepersonal = () => {
  const id = uuidv4();

  let temp = "";

  id.split("-").forEach((item) => (temp += item));

  return temp;
};

const randomnumber = (min, max) => {
  const chance = new Chance();
  return chance.integer({ min, max });
};

const generaterandomnumber = (length) => {
  const chance = new Chance();
  return chance.string({ length, pool: "123456789" });
};

const randomstring = (length) => {
  const chance = new Chance();
  return chance.string({ length, pool: "abcdefghqrtwxy" });
};

module.exports = {
  generatecode,
  generatecodepersonal,
  randomnumber,
  generaterandomnumber,
  randomstring,
};
