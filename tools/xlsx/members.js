const path = require('path')
const XLSX = require("xlsx");
const code = require("../code");

async function xlsxmembers() {
  return new Promise((resolve, reject) => {
    const pdfname = code();
    const pathtemp = path.join(__dirname, `../../temp/${pdfname}.xlsx`);

    const data = [];
    const workSheet = XLSX.utils.json_to_sheet(data);
    const workBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workBook, workSheet, "Miembros");

    XLSX.writeFile(workBook, pathtemp);

    resolve(pathtemp)
  });
}

module.exports = { xlsxmembers };
