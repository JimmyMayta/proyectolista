const moment = require("moment-timezone");
const Chance = require("chance");

const code = () => {
  const chance = new Chance();
  return `${moment()
    .tz("America/La_Paz")
    .format("YYYYMMDDHHmmssSSS")}0${chance.string({
    length: 3,
    pool: "123456789",
  })}`;
};

module.exports = code;
