const PDFDocument = require("pdfkit");
const fs = require("fs");
const path = require("path");
const code = require("../code");

async function pdfmembers() {
  return new Promise((resolve, reject) => {
    const pdfname = code();
    const pathtemp = path.join(__dirname, `../../temp/${pdfname}.pdf`);

    const fontLatoRegular = path.join(__dirname, "fonts/Lato-Regular.ttf");
    const fontLatoBold = path.join(__dirname, "fonts/Lato-Bold.ttf");
    const imageLogo = path.join(__dirname, "images/iebs.png");
    const Blue900Primary = "#0d47a1";
    const Blue900Dark = "#002171";

    const doc = new PDFDocument({ size: "LETTER" });
    doc.lineWidth(1);
    doc
      .lineJoin("round")
      .roundedRect(10, 10, 590, 770, 5)
      .stroke(Blue900Primary);
    doc.image(imageLogo, 15, 15, {
      fit: [30, 30],
      align: "center",
      valign: "center",
    });
    doc.fillColor(Blue900Primary);
    doc.font(fontLatoBold).fontSize(19);
    doc.text("Lista de Miembros", 15, 15, {
      width: 590,
      align: "center",
    });

    doc.font(fontLatoRegular).fontSize(12);
    doc.text('texto grupo', 30, 30, {
      width: 590,
      align: "center",
    });

    writeStream = fs.createWriteStream(pathtemp);
    doc.pipe(writeStream);
    doc.end();

    writeStream.on("finish", () => {
      resolve(pathtemp);
    });
  });
}

module.exports = { pdfmembers };
