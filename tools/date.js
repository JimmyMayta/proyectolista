const moment = require("moment-timezone");

const date = () => {
  return moment().tz("America/La_Paz").format("YYYY-MM-DD HH:mm:ss.SSS");
};
module.exports = date;
