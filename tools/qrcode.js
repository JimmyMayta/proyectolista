const path = require("path");
const fs = require("fs");
const sharp = require("sharp");
const QRCode = require("qrcode");

const qrcode = async (image, data) => {
  const imageqr = path.join(__dirname, `../public/images/qrcode/${image}.png`);

  await QRCode.toFile(imageqr, data, {
    type: "png",
    margin: 1,
    width: 500,
    color: {
      dark: "#000",
      light: "#FFF",
    },
    errorCorrectionLevel: "L",
  });

  const {
    format: imageextension,
    width: imagewidth,
    height: imageheight,
  } = await sharp(imageqr).metadata();

  return { imageextension, imagewidth, imageheight };
};

module.exports = qrcode;
