const { request, response } = require("express");
const MemberModel = require("../models/member");
const DataModel = require("../models/data");
const MainImageModel = require("../models/mainimage");
const session = require("../tools/session");
const datacontrol = require("../data/datacontrol");

const main = async (req = request, res = response) => {
  const biblicaltext = [
    {
      book: "Proverbios",
      chapter: "3",
      verse: "5",
      text: "Fíate de Jehová de todo tu corazón, Y no te apoyes en tu propia prudencia",
    },
    {
      book: "Apocalipsis",
      chapter: "21",
      verse: "4",
      text: "Enjugará Dios toda lágrima de los ojos de ellos; y ya no habrá muerte, ni habrá más llanto, ni clamor, ni dolor; porque las primeras cosas pasaron.",
    },
    {
      book: "Salmos",
      chapter: "62",
      verse: "1",
      text: "Al músico principal; a Jedutún. Salmo de David. En Dios solamente está acallada mi alma; De él viene mi salvación.",
    },
    {
      book: "2 Corintios",
      chapter: "12",
      verse: "9",
      text: "Y me ha dicho: Bástate mi gracia; porque mi poder se perfecciona en la debilidad. Por tanto, de buena gana me gloriaré más bien en mis debilidades, para que repose sobre mí el poder de Cristo.",
    },
    {
      book: "Proverbios",
      chapter: "22",
      verse: "6",
      text: "Instruye al niño en su camino, Y aun cuando fuere viejo no se apartará de él.",
    },
    {
      book: "Eclesiastés",
      chapter: "3",
      verse: "1",
      text: "Todo tiene su tiempo, y todo lo que se quiere debajo del cielo tiene su hora.",
    },
    {
      book: "2 Crónicas",
      chapter: "7",
      verse: "14",
      text: "si se humillare mi pueblo, sobre el cual mi nombre es invocado, y oraren, y buscaren mi rostro, y se convirtieren de sus malos caminos; entonces yo oiré desde los cielos, y perdonaré sus pecados, y sanaré su tierra.",
    },
  ];

  const sessioncode = await session(req);

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const mainimage = await MainImageModel.find({ state: "active" });
  const maintitle = await DataModel.findOne({
    data: datacontrol.maintitle,
    state: "active",
  });
  const maindescription = await DataModel.findOne({
    data: datacontrol.maindescription,
    state: "active",
  });
  const mainactivitiestitle = await DataModel.findOne({
    data: datacontrol.mainactivitiestitle,
    state: "active",
  });
  const mainactivitiesdescription = await DataModel.findOne({
    data: datacontrol.mainactivitiesdescription,
    state: "active",
  });

  return res.render("main/main", {
    active: "main",
    session: sessioncode,
    member,
    mainimage,
    maintitle: maintitle !== null ? maintitle : false,
    maindescription: maindescription !== null ? maindescription : false,
    mainactivitiestitle: mainactivitiestitle !== null ? mainactivitiestitle : false,
    mainactivitiesdescription: mainactivitiesdescription !== null ? mainactivitiesdescription : false,
  });
};

const maincontrol = async (req = request, res = response) => {
  const maindata = [
    {
      id: 1,
      title: "First slide label",
      description: "Ambientes apriados para todo tipo de uso",
      image: "20220219193001.jpg",
    },
    {
      id: 2,
      title: "Segundo imagen",
      description: "Docntes altamente calificados",
      image: "20220219193002.jpg",
    },
    {
      id: 3,
      title: "Tercera imagen",
      description: "Estudiante estudiando a un nivel avanzado",
      image: "20220219193003.jpg",
    },
  ];

  const biblicaltext = [
    {
      book: "Proverbios",
      chapter: "3",
      verse: "5",
      text: "Fíate de Jehová de todo tu corazón, Y no te apoyes en tu propia prudencia",
    },
    {
      book: "Apocalipsis",
      chapter: "21",
      verse: "4",
      text: "Enjugará Dios toda lágrima de los ojos de ellos; y ya no habrá muerte, ni habrá más llanto, ni clamor, ni dolor; porque las primeras cosas pasaron.",
    },
    {
      book: "Salmos",
      chapter: "62",
      verse: "1",
      text: "Al músico principal; a Jedutún. Salmo de David. En Dios solamente está acallada mi alma; De él viene mi salvación.",
    },
    {
      book: "2 Corintios",
      chapter: "12",
      verse: "9",
      text: "Y me ha dicho: Bástate mi gracia; porque mi poder se perfecciona en la debilidad. Por tanto, de buena gana me gloriaré más bien en mis debilidades, para que repose sobre mí el poder de Cristo.",
    },
    {
      book: "Proverbios",
      chapter: "22",
      verse: "6",
      text: "Instruye al niño en su camino, Y aun cuando fuere viejo no se apartará de él.",
    },
    {
      book: "Eclesiastés",
      chapter: "3",
      verse: "1",
      text: "Todo tiene su tiempo, y todo lo que se quiere debajo del cielo tiene su hora.",
    },
    {
      book: "2 Crónicas",
      chapter: "7",
      verse: "14",
      text: "si se humillare mi pueblo, sobre el cual mi nombre es invocado, y oraren, y buscaren mi rostro, y se convirtieren de sus malos caminos; entonces yo oiré desde los cielos, y perdonaré sus pecados, y sanaré su tierra.",
    },
  ];

  const sessioncode = await session(req);

  const { code } = req.params;

  if (code) {
    console.log(code);
  } else {
    console.log("errro");
  }

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  switch (code) {
    case "nosotros":
      return res.render("main/nosotros", {
        active: "nosotros",
        session: sessioncode,
        member,
      });
      break;
    case "contact":
      return res.render("main/contact", {
        active: "contact",
        session: sessioncode,
        member,
      });
      break;
  }
};

const activities = async (req = request, res = response) => {
  const sessioncode = await session(req);

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("main/activities", {
    active: "activities",
    session: sessioncode,
    member: member,
  });
};

const nosotros = async (req = request, res = response) => {
  const sessioncode = await session(req);

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("main/nosotros", {
    active: "nosotros",
    session: sessioncode,
    member,
  });
};

const contact = async (req = request, res = response) => {
  const sessioncode = await session(req);

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("main/contact", {
    active: "contact",
    session: sessioncode,
    member,
  });
};

module.exports = { main, maincontrol, activities, nosotros, contact };
