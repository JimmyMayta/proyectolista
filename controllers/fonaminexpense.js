const { request, response } = require("express");
const MemberModel = require("../models/member");
const session = require("../tools/session");
const date = require("../tools/date");
const code = require("../tools/code");
const { sum } = require("../tools/mathematic");
const math = require("mathjs");

const expense = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const [member] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
  ]);

  return res.render("fonamin/expense", {
    active: "expense",
    session: sessioncode,
    member,
  });
};


module.exports = {
  expense,
};
