const session = require("../tools/session");
const MemberModel = require("../models/member");
const MainImageModel = require("../models/mainimage");
const DataModel = require("../models/data");
const IncomeModel = require("../models/income");
const ExpenseModel = require("../models/expense");
const {
  controlmainimage,
  controlmainimagedelete,
} = require("../tools/images/controlmainimage");
const datacontrol = require("../data/datacontrol");
const date = require("../tools/date");
const code = require("../tools/code");

const controlmain = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );
  const mainimage = await MainImageModel.find();
  const maintitle = await DataModel.findOne({
    data: datacontrol.maintitle,
    state: "active",
  });
  const maindescription = await DataModel.findOne({
    data: datacontrol.maindescription,
    state: "active",
  });
  const mainactivitiestitle = await DataModel.findOne({
    data: datacontrol.mainactivitiestitle,
    state: "active",
  });
  const mainactivitiesdescription = await DataModel.findOne({
    data: datacontrol.mainactivitiesdescription,
    state: "active",
  });

  return res.render("maincontrol/controlmain", {
    active: "controlmain",
    session: sessioncode,
    member,
    mainimage,
    maintitle: maintitle !== null ? maintitle : false,
    maindescription: maindescription !== null ? maindescription : false,
    mainactivitiestitle:
      mainactivitiestitle !== null ? mainactivitiestitle : false,
    mainactivitiesdescription:
      mainactivitiesdescription !== null ? mainactivitiesdescription : false,
  });
};

const controlmaincreate = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { title, description } = req.body;
  const { image: imagemain } = req.files;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );
  const { image, imagename, imageextension, imagewidth, imageheight } =
    await controlmainimage(imagemain);

  const mainimage = new MainImageModel({
    member: member._id,
    title,
    image,
    imagename,
    imageextension,
    imagewidth,
    imageheight,
    description,
    creationdate: date(),
  });
  await mainimage.save();
  console.log(mainimage);

  return res.redirect("/control/main");
};

const controlmainupdate = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { maintitle, maindescription } = req.body;
  console.log({ maintitle, maindescription });

  const controlmaintitle = await DataModel.findOne({
    data: datacontrol.maintitle,
  });
  const controlmaindescription = await DataModel.findOne({
    data: datacontrol.maindescription,
  });

  if (controlmaintitle === null) {
    const title = new DataModel({
      data: datacontrol.maintitle,
      dataname: maintitle,
    });
    await title.save();
  } else {
    await DataModel.findOneAndUpdate(
      { data: datacontrol.maintitle },
      {
        dataname: maintitle,
      }
    );
  }

  if (controlmaindescription === null) {
    const description = new DataModel({
      data: datacontrol.maindescription,
      dataname: maindescription,
    });
    await description.save();
  } else {
    await DataModel.findOneAndUpdate(
      { data: datacontrol.maindescription },
      {
        dataname: maindescription,
      }
    );
  }

  return res.redirect("/control/main");
};

const controlmaindelete = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { id } = req.params;

  const resp = await MainImageModel.findById(id);

  await controlmainimagedelete(resp.image);
  await MainImageModel.findByIdAndDelete(resp._id);

  return res.redirect("/control/main");
};

const controlmainactivitiesupdate = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { mainactivitiestitle, mainactivitiesdescription } = req.body;

  const controlmainactivitiestitle = await DataModel.findOne({
    data: datacontrol.mainactivitiestitle,
  });
  const controlmainactivitiesdescription = await DataModel.findOne({
    data: datacontrol.mainactivitiesdescription,
  });

  if (controlmainactivitiestitle === null) {
    const title = new DataModel({
      data: datacontrol.mainactivitiestitle,
      dataname: mainactivitiestitle,
    });
    await title.save();
  } else {
    await DataModel.findOneAndUpdate(
      { data: datacontrol.mainactivitiestitle },
      {
        dataname: mainactivitiestitle,
      }
    );
  }

  if (controlmainactivitiesdescription === null) {
    const description = new DataModel({
      data: datacontrol.mainactivitiesdescription,
      dataname: mainactivitiesdescription,
    });
    await description.save();
  } else {
    await DataModel.findOneAndUpdate(
      { data: datacontrol.mainactivitiesdescription },
      {
        dataname: mainactivitiesdescription,
      }
    );
  }

  return res.redirect("/control/main");
};

const controlactivities = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("maincontrol/controlactivities", {
    active: "controlactivities",
    session: sessioncode,
    member,
  });
};

const controlnosotros = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("maincontrol/controlnosotros", {
    active: "controlnosotros",
    session: sessioncode,
    member,
  });
};

const controlcontact = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("maincontrol/controlcontact", {
    active: "controlcontact",
    session: sessioncode,
    member,
  });
};

const controlfonamin = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const incomes = await IncomeModel.find({
    $or: [{ state: "active" }, { state: "inactive" }],
  });
  const expenses = await ExpenseModel.find({
    $or: [{ state: "active" }, { state: "inactive" }],
  });

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("maincontrol/controlfonamin", {
    active: "controlfonamin",
    session: sessioncode,
    member,
    incomes,
    expenses,
  });
};

const controlfonaminincomecreate = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { income } = req.body;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const incomes = await IncomeModel.findOne({
    incomename: income,
  });

  if (incomes === null) {
    const incomes = new IncomeModel({
      incomecode: code(),
      income: income.replace(/\s+/g, ""),
      incomename: income,
      member: member._id,
      creationdate: date(),
    });
    await incomes.save();
  } else {
    if (incomes.state === "deleted") {
      await IncomeModel.findByIdAndUpdate(incomes._id, {
        state: "active",
        member: member._id,
        updatedate: date(),
      });
    }
  }

  return res.redirect("/control/fonamin");
};

const controlfonaminincomeupdate = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }
  const { id } = req.params;
  const { income } = req.body;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const incomes = await IncomeModel.findOne({
    incomename: income,
  });

  if (incomes === null) {
    await IncomeModel.findOneAndUpdate(
      {
        _id: id,
        $or: [{ state: "active" }, { state: "inactive" }],
      },
      {
        income: income.replace(/\s+/g, ""),
        incomename: income,
        member: member._id,
        updatedate: date(),
      }
    );
  }

  return res.redirect("/control/fonamin");
};

const controlfonaminincomedelete = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }
  const { id } = req.params;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  await IncomeModel.findOneAndUpdate(
    {
      _id: id,
      $or: [{ state: "active" }, { state: "inactive" }],
    },
    {
      member: member._id,
      state: "deleted",
      deletedate: date(),
    }
  );

  return res.redirect("/control/fonamin");
};

const controlfonaminincomestates = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }
  const { id } = req.params;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const incomes = await IncomeModel.findById(id);

  await IncomeModel.findByIdAndUpdate(id, {
    state: incomes.state === "active" ? "inactive" : "active",
    member: member._id,
    updatedate: date(),
  });

  return res.redirect("/control/fonamin");
};

const controlfonaminexpensecreate = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { expense } = req.body;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const expenses = await ExpenseModel.findOne({
    expensename: expense,
  });

  if (expenses === null) {
    const expensemodel = new ExpenseModel({
      expensecode: code(),
      expense: expense.replace(/\s+/g, ""),
      expensename: expense,
      member: member._id,
      creationdate: date(),
    });
    await expensemodel.save();
  } else {
    if (expenses.state === "deleted") {
      await ExpenseModel.findByIdAndUpdate(expenses._id, {
        state: "active",
        member: member._id,
        updatedate: date(),
      });
    }
  }

  return res.redirect("/control/fonamin");
};

const controlfonaminexpenseupdate = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }
  const { id } = req.params;
  const { expense } = req.body;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const expenses = await ExpenseModel.findOne({
    expensename: expense,
  });

  if (expenses === null) {
    await ExpenseModel.findOneAndUpdate(
      {
        _id: id,
        $or: [{ state: "active" }, { state: "inactive" }],
      },
      {
        expense: expense.replace(/\s+/g, ""),
        expensename: expense,
        member: member._id,
        updatedate: date(),
      }
    );
  }

  return res.redirect("/control/fonamin");
};

const controlfonaminexpensedelete = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }
  const { id } = req.params;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  await ExpenseModel.findOneAndUpdate(
    {
      _id: id,
      $or: [{ state: "active" }, { state: "inactive" }],
    },
    {
      member: member._id,
      state: "deleted",
      deletedate: date(),
    }
  );

  return res.redirect("/control/fonamin");
};

const controlfonaminexpensestates = async (req, res) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }
  const { id } = req.params;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const expenses = await ExpenseModel.findById(id);

  await ExpenseModel.findByIdAndUpdate(id, {
    state: expenses.state === "active" ? "inactive" : "active",
    member: member._id,
    updatedate: date(),
  });

  return res.redirect("/control/fonamin");
};

module.exports = {
  controlmain,
  controlmaincreate,
  controlmainupdate,
  controlmaindelete,
  controlmainactivitiesupdate,
  controlactivities,
  controlnosotros,
  controlcontact,
  controlfonamin,
  controlfonaminincomecreate,
  controlfonaminincomeupdate,
  controlfonaminincomedelete,
  controlfonaminincomestates,
  controlfonaminexpensecreate,
  controlfonaminexpenseupdate,
  controlfonaminexpensedelete,
  controlfonaminexpensestates,
};
