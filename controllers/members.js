const fs = require("fs");
const { request, response } = require("express");
const MemberModel = require("../models/member");
const Church = require("../models/church");
const ChurchMember = require("../models/churchmember");
const MemberChurch = require("../models/memberchurch");
const MemberImage = require("../models/memberimage");
const MemberImageCodeQR = require("../models/memberimagecodeqr");
const { production, urlpro, url } = require("../environments/environment");
const session = require("../tools/session");
const date = require("../tools/date");
const code = require("../tools/code");
const qrcode = require("../tools/qrcode");
const { pdfmembers } = require("../tools/pdf/members");
const { xlsxmembers } = require("../tools/xlsx/members");

const members = async (req = request, res = response) => {
  const sessioncode = await session(req);
  const { search = 1, pagination = 1 } = req.params;

  if (!sessioncode) {
    return res.redirect("/");
  }

  if (!isNaN(Number(search))) {
    let numeropagination = !isNaN(Number(search)) ? Number(search) : 1;

    const totalmember = await MemberModel.countDocuments({
      $or: [{ state: "active" }, { state: "inactive" }],
    });
    let totalmodulo = totalmember % 10;
    let respuesta = totalmember - totalmodulo;
    let totalpagination = respuesta / 10;

    if (totalmodulo !== 0) {
      totalpagination = totalpagination + 1;
    }

    let temp = numeropagination * 10;
    let respag = temp - 10;

    const member = await MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    );

    const members = await MemberModel.aggregate([
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          state: 1,
          identitynumber: 1,
        },
      },
      {
        $lookup: {
          from: "memberimage",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { imagename: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
      {
        $lookup: {
          from: "memberimagecodeqr",
          localField: "_id",
          foreignField: "member",
          as: "imagecode",
          pipeline: [{ $project: { image: 1 } }],
        },
      },
      {
        $unwind: "$imagecode",
      },
      { $skip: respag },
      { $limit: 10 },
    ]);

    const churches = await Church.find(
      { $or: [{ state: "active" }, { state: "inactive" }] },
      {
        _id: 1,
        church: 1,
      }
    );

    return res.render("members/members", {
      active: "members",
      session: sessioncode,
      member,
      members,
      churches,
      totalmember,
      totalpagination,
      numeropagination,
      search: "",
    });
  } else {
    const members2 = await MemberModel.aggregate([
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          state: 1,
          identitynumber: 1,
        },
      },
      {
        $lookup: {
          from: "memberimage",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { image: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
      {
        $lookup: {
          from: "memberimagecodeqr",
          localField: "_id",
          foreignField: "member",
          as: "imagecode",
          pipeline: [{ $project: { image: 1 } }],
        },
      },
      {
        $unwind: "$imagecode",
      },
    ]);

    let memberslist = [];

    members2.forEach((item) => {
      if (
        item.names.includes(search.toLowerCase()) ||
        item.firstlastname.includes(search.toLowerCase()) ||
        item.secondlastname.includes(search.toLowerCase())
      ) {
        memberslist.push(item);
      }
    });

    let numeropagination = 1;

    const totalmember = memberslist.length;
    // console.log(totalmember);

    let totalmodulo = totalmember % 10;
    let respuesta = totalmember - totalmodulo;
    let totalpagination = respuesta / 10;

    if (totalmodulo !== 0) {
      totalpagination = totalpagination + 1;
    }

    let temp = numeropagination * 10;
    let respag = temp - 10;

    const member = await MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    );

    const members = await MemberModel.aggregate([
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          state: 1,
          identitynumber: 1,
        },
      },
      {
        $lookup: {
          from: "memberimage",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { image: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
      {
        $lookup: {
          from: "memberimagecodeqr",
          localField: "_id",
          foreignField: "member",
          as: "imagecode",
          pipeline: [{ $project: { image: 1 } }],
        },
      },
      {
        $unwind: "$imagecode",
      },
      { $skip: respag },
      { $limit: 10 },
    ]);

    const churches = await Church.find(
      { $or: [{ state: "active" }, { state: "inactive" }] },
      {
        _id: 1,
        church: 1,
      }
    );

    return res.render("members/members", {
      active: "members",
      session: sessioncode,
      member,
      members,
      churches,
      totalmember,
      totalpagination,
      numeropagination,
      search,
    });
  }
};

const memberspdf = async (req = request, res = response) => {
  const filepdf = await pdfmembers();
  res.setHeader("Content-Disposition", 'inline;filename="miembros.pdf"');
  fs.createReadStream(filepdf).pipe(res);
};

const membersxlsx = async (req = request, res = response) => {
  const filexlsx = await xlsxmembers();
  res.download(filexlsx, "miembros.xlsx");
};

const memberscreate = async (req = request, res = response) => {
  const {
    names,
    firstlastname,
    secondlastname,
    gender,
    identitynumber,
    email,
    cellular,
    telephone,
    photography,
    birthdate,
    province,
    department,
    country,
    married,
    church,
  } = req.body;

  console.log(names, firstlastname, secondlastname);

  const sessioncode = await session(req);
  const codigo = code();

  if (!sessioncode) {
    return res.redirect("/");
  }

  const [member, memberexists] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    MemberModel.findOne(
      { names, firstlastname, secondlastname, gender },
      {
        _id: 1,
        membercode: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
  ]);

  if (memberexists) {
    const iglesiamiembro = new ChurchMember({
      church: church,
      member: memberexists._id,
      creationdate: date(),
    });
    await iglesiamiembro.save();
  } else {
    try {
      const miembro = new MemberModel({
        code: codigo,
        user: null,
        password: null,
        names,
        firstlastname: firstlastname ? firstlastname : "",
        secondlastname: secondlastname ? secondlastname : "",
        gender,
        identitynumber,
        email,
        cellular,
        telephone,
        photography,
        birthdate,
        province,
        department,
        country,
        married,
        member: member._id,
        creationdate: date(),
      });
      await miembro.save();

      const iglesiamiembro = new ChurchMember({
        church,
        member: miembro._id,
        creationdate: date(),
      });
      await iglesiamiembro.save();

      const memberchurch = new MemberChurch({
        member: miembro._id,
        creationdate: date(),
      });
      await memberchurch.save();

      const memberimage = new MemberImage({
        member: miembro._id,
        image: null,
        imagename: null,
        imageextension: null,
        imagewidth: null,
        imageheight: null,
        creationdate: date(),
      });
      await memberimage.save();

      const urldata = production ? urlpro : url;
      const { imageextension, imagewidth, imageheight } = await qrcode(
        codigo,
        `${urldata}/${codigo}`
      );

      const memberimageqr = MemberImageCodeQR({
        member: miembro._id,
        image: codigo,
        imagename: `${codigo}.png`,
        imageextension,
        imagewidth,
        imageheight,
        creationdate: date(),
      });
      await memberimageqr.save();
    } catch (error) {
      return res.redirect("/members");
    }
  }

  return res.redirect("/members");
};

module.exports = { members, memberspdf, membersxlsx, memberscreate };
