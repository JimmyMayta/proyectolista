const { request, response } = require("express");
const MemberModel = require("../models/member");
const DepartmentModel = require("../models/department");
const CountryModel = require("../models/country");
const session = require("../tools/session");
const date = require("../tools/date");

const department = async (req = request, res = response) => {
  const sessionmember = await session(req);

  if (!sessionmember) {
    return res.redirect("/");
  }

  const [member, departments, countries] = await Promise.all([
    MemberModel.findOne(
      { code: sessionmember, state: "active" },
      {
        _id: 0,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    DepartmentModel.find(
      { state: "active" },
      {
        _id: 1,
        department: 1,
        abbreviation: 1,
      }
    ).populate("country", ["_id", "country"]),
    CountryModel.find(
      { state: "active" },
      {
        _id: 1,
        country: 1,
        state: 1,
      }
    ),
  ]);

  return res.render("department/department", {
    active: "district",
    session: sessionmember,
    member,
    departments,
    countries,
    menu: "department",
  });
};

const departmentcreate = async (req = request, res = response) => {
  const sessionmember = await session(req);

  if (!sessionmember) {
    return res.redirect("/");
  }

  const { department, abbreviation, country } = req.body;

  const member = await MemberModel.findOne(
    { code: sessionmember, state: "active" },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const departamento = new DepartmentModel({
    department,
    abbreviation,
    country,
    member: member._id,
    creationdate: date(),
  });
  await departamento.save();

  return res.redirect("/department");
};

module.exports = {
  department,
  departmentcreate,
};
