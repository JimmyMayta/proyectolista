const { response, request } = require("express");
const MemberModel = require("../models/member");

const login = async (req = request, res = response) => {
  const { password } = req.body;

  try {
    const miembro = await MemberModel.findOne(
      {
        identitynumber:password,
        state: "active",
      },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    );

    if (miembro) {
      req.session.session = miembro.code;
    }
  } catch (error) {
    console.log("Error Login");
  }

  return res.redirect("/");
};

const logout = (req = request, res = response) => {
  req.session = null
  return res.redirect("/");
};

module.exports = {
  login,
  logout,
};
