const { request, response } = require("express");
const MemberModel = require("../models/member");
const session = require("../tools/session");

const church = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { code } = req.params;

  const [member, memberselect] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    MemberModel.aggregate([
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          state: 1,
          identitynumber: 1,
        },
      },
      {
        $match: { code, state: "active" },
      },
      {
        $lookup: {
          from: "memberimages",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { _id: 0, image: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
    ]),
  ]);

  return res.render("member/memberpersonal", {
    active: "member",
    session: sessioncode,
    member,
    memberselect: memberselect[0],
  });
};

module.exports = { church };
