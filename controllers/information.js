const { request, response } = require("express");
const MemberModel = require("../models/member");
const session = require("../tools/session");
const informationdata = require("../data/information");

const information = async (req = request, res = response) => {
  const sessioncode = await session(req);

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("information/information", {
    active: "information",
    session: sessioncode,
    member,
    information: informationdata,
  });
};

module.exports = { information };
