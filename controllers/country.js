const { request, response } = require("express");
const MemberModel = require("../models/member");
const CountryModel = require("../models/country");
const session = require('../tools/session')
const date = require('../tools/date')

const country = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const [member, countries] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    CountryModel.find(
      { $or: [{ state: "active" }, { state: "inactive" }] },
      {
        _id: 1,
        country: 1,
        state: 1,
      }
    ),
  ]);

  return res.render("country/country", {
    active: "district",
    session: sessioncode,
    member,
    countries,
    menu: "country",
  });
};

const countrycreate = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { country } = req.body;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const pais = new CountryModel({
    country,
    member: member._id,
    creationdate: date(),
  });
  await pais.save();

  return res.redirect("/country");
};

const countrystate = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { id, state } = req.params;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  await CountryModel.findByIdAndUpdate(
    { _id: id },
    { state, updatedate: date() }
  );

  return res.redirect("/country");
};

module.exports = {
  country,
  countrycreate,
  countrystate,
};
