const { request, response } = require("express");
const MemberModel = require("../models/member");
const IncomeModel = require("../models/income");
const MemberIncomeModel = require("../models/memberincome");
const session = require("../tools/session");
const MemberIncomeDataModel = require("../models/memberincomedata");
const date = require("../tools/date");
const code = require("../tools/code");
const { sum } = require("../tools/mathematic");
const math = require("mathjs");

const income = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const [member, incomes] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    IncomeModel.find({ state: "active" }),
  ]);

  return res.render("fonamin/income", {
    active: "income",
    session: sessioncode,
    member,
    incomes,
  });
};

const incomemember = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { code } = req.params;

  const [member, income, members] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    IncomeModel.findOne({
      incomecode: code,
      state: "active",
    }),
    MemberModel.find(
      { state: "active" },
      {
        _id: 0,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        identitynumber: 1,
      }
    ),
  ]);

  const memberincome = await MemberIncomeModel.find({
    income: income._id,
    state: "active",
  }).populate("member", [
    "code",
    "names",
    "firstlastname",
    "secondlastname",
    "identitynumber",
    "state",
  ]);

  console.log(income);

  return res.render("fonamin/incomemember", {
    active: "income",
    session: sessioncode,
    member,
    members: JSON.stringify(members),
    income,
    memberincome,
  });
};

const incomemembercreate = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { code } = req.params;
  const { membercode } = req.body;

  const memberver = await MemberModel.findOne({
    code: membercode,
    state: "active",
  });

  if (memberver === null) {
    return res.redirect(`/fonamin/income/${code}`);
  }

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );
  const memberselecionado = await MemberModel.findOne({ code: membercode });
  const incomeseleccionado = await IncomeModel.findOne({ incomecode: code });
  const verificar = await MemberIncomeModel.findOne({
    member: memberselecionado._id,
  });

  if (verificar === null) {
    const guardar = new MemberIncomeModel({
      member: memberselecionado._id,
      income: incomeseleccionado._id,
      membermember: member._id,
      creationdate: date(),
    });
    await guardar.save();
  }

  return res.redirect(`/fonamin/income/${code}`);
};

const incomememberdata = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { codeincome, codemember } = req.params;

  const [member, memberselected, incomeselected] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    MemberModel.findOne({ code: codemember, state: "active" }),
    IncomeModel.findOne({ incomecode: codeincome, state: "active" }),
  ]);

  const memberincomeselected = await MemberIncomeModel.findOne({
    member: memberselected._id,
    income: incomeselected._id,
  });

  const memberincomedata = await MemberIncomeDataModel.find({
    member: memberselected._id,
    income: incomeselected._id,
    memberincome: memberincomeselected._id,
    state: "active",
  });

  const total = await sum(memberincomedata.map((e) => e.amount));

  return res.render("fonamin/incomememberdata", {
    active: "income",
    session: sessioncode,
    member,
    incomeselected,
    memberselected,
    memberincomedata: memberincomedata,
    total: math.round(total, 2).toFixed(2),
  });
};

const incomememberdatacreate = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { codeincome, codemember } = req.params;
  const { amount } = req.body;

  const [member, memberselected, incomeselected] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    MemberModel.findOne({ code: codemember, state: "active" }),
    IncomeModel.findOne({ incomecode: codeincome, state: "active" }),
  ]);

  const memberincomeselected = await MemberIncomeModel.findOne({
    member: memberselected._id,
    income: incomeselected._id,
  });

  const memberincomedata = new MemberIncomeDataModel({
    memberincomedatacode: code(),
    member: memberselected._id,
    income: incomeselected._id,
    memberincome: memberincomeselected._id,
    membermember: member._id,
    amount: math.round(amount, 2).toFixed(2),
    creationdate: date(),
  });
  await memberincomedata.save();

  return res.redirect(
    `/fonamin/income/${incomeselected.incomecode}/${memberselected.code}`
  );
};

const incomememberdataupdate = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { codeincome, codemember, codeincomemembercode } = req.params;
  const { amount } = req.body;

  const [member] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode, state: "active" },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
  ]);

  await MemberIncomeDataModel.findOneAndUpdate(
    { memberincomedatacode: codeincomemembercode },
    {
      amount: math.round(amount, 2).toFixed(2),
      membermember: member._id,
      updatedate: date(),
    }
  );

  return res.redirect(`/fonamin/income/${codeincome}/${codemember}`);
};

const incomememberdatadelete = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { codeincome, codemember, codeincomemembercode } = req.params;

  const [member] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode, state: "active" },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
  ]);

  await MemberIncomeDataModel.findOneAndUpdate(
    { memberincomedatacode: codeincomemembercode },
    {
      membermember: member._id,
      state: "deleted",
      deletedate: date(),
    }
  );

  return res.redirect(`/fonamin/income/${codeincome}/${codemember}`);
};

module.exports = {
  income,
  incomemember,
  incomemembercreate,
  incomememberdata,
  incomememberdatacreate,
  incomememberdataupdate,
  incomememberdatadelete,
};
