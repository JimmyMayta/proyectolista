const fs = require("fs");
const { request, response } = require("express");
const MemberModel = require("../models/member");
const ChurchModel = require("../models/church");
const DistrictModel = require("../models/district");
const session = require("../tools/session");
const date = require("../tools/date");
const { pdfreports } = require("../tools/pdf/reports");

const reports = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const member = await MemberModel.find(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  return res.render("reports/reports", {
    active: "reports",
    session: sessioncode,
    member: member[0],
  });
};

const reportspdf = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const filepdf = await pdfreports();
  res.setHeader("Content-Disposition", 'inline;filename="miembros.pdf"');
  fs.createReadStream(filepdf).pipe(res);
};

module.exports = { reports, reportspdf };
