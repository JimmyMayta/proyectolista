const { request, response } = require("express");
const MemberModel = require("../models/member");
const MemberChurchModel = require("../models/memberchurch");
const MemberImageModel = require("../models/memberimage");
const MemberImageCodeQRModel = require("../models/memberimagecodeqr");
const MemberAccessModel = require("../models/memberaccess");
const DepartmentModel = require("../models/department");
const CountryModel = require("../models/country");
const AccessModel = require("../models/access");
const MemberSubAccessModel = require("../models/membersubaccess");
const LevelModel = require("../models/level");
const code = require("../tools/code");
const session = require("../tools/session");
const date = require("../tools/date");
const qrcode = require("../tools/qrcode");
const imageupload = require("../tools/images/member");
const { imagecredential } = require("../tools/images/credential");
const { generaterandomnumber, randomstring } = require("../tools/libraries");
const {
  production,
  membercreatekey,
  url,
  urlpro,
} = require("../environments/environment");
const data = require("../data/data");

const member = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { code } = req.params;

  const [member, memberselect, departments, countries] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    // MemberModel.aggregate([
    //   { $match: { code: sessioncode } },
    //   {
    //     $project: {
    //       _id: 1,
    //       code: 1,
    //       names: 1,
    //       firstlastname: 1,
    //       secondlastname: 1,
    //       state: 1,
    //     },
    //   },
    // ]),
    MemberModel.aggregate([
      {
        $match: { code },
      },
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          gender: 1,
          identitynumber: 1,
          email: 1,
          cellular: 1,
          birthdate: 1,
          department: 1,
          country: 1,
          member: 1,
          state: 1,
        },
      },
      {
        $lookup: {
          from: "memberimage",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { imagename: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
      {
        $lookup: {
          from: "memberimagecodeqr",
          localField: "_id",
          foreignField: "member",
          as: "imagecode",
          pipeline: [{ $project: { imagename: 1 } }],
        },
      },
      {
        $unwind: "$imagecode",
      },
    ]),
    DepartmentModel.find({ $or: [{ state: "active" }, { state: "inactive" }] }),
    CountryModel.find({ $or: [{ state: "active" }, { state: "inactive" }] }),
  ]);

  // console.log(member);

  if (memberselect.length === 0) {
    return res.redirect("/members");
  }

  // console.log(memberselect);

  return res.render("member/memberpersonal", {
    active: "member",
    tab: "member",
    session: sessioncode,
    member,
    memberselect: memberselect[0],
    departments,
    countries,
  });
};

const memberimageupdate = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { code } = req.params;

  const member = await MemberModel.findOne({ code });

  try {
    const { image, imagename, imageextension, imagewidth, imageheight } =
      await imageupload(member.code, req.files);

    await MemberImageModel.findOneAndUpdate(
      { member: member._id },
      {
        image,
        imagename,
        imageextension,
        imagewidth,
        imageheight,
        updatedate: date(),
      }
    );
  } catch (error) {
    console.log(error);
  }

  return res.redirect(`/member/${code}`);
};

const memberupdate = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { code } = req.params;
  const {
    names,
    firstlastname,
    secondlastname,
    gender,
    identitynumber,
    email,
    cellular,
    telephone,
    photography,
    birthdate,
    province,
    department,
    country,
    married,
  } = req.body;

  console.log({
    names,
    firstlastname,
    secondlastname,
  });

  await MemberModel.findOneAndUpdate(
    { code },
    {
      names: names === "" ? "" : names,
      firstlastname: firstlastname === "" ? "" : firstlastname,
      secondlastname: secondlastname === "" ? "" : secondlastname,
      gender: gender === "" ? null : gender,
      identitynumber: identitynumber === "" ? null : identitynumber,
      email: email === "" ? null : email,
      cellular: cellular === "" ? null : cellular,
      telephone: telephone === "" ? null : telephone,
      photography: photography === "" ? null : photography,
      birthdate: birthdate === "" ? null : birthdate,
      province: province === "" ? null : province,
      department: department === "" ? null : department,
      country: country === "" ? null : country,
      married: married === "" ? null : married,
      updatedate: date(),
    }
  );

  return res.redirect(`/member/${code}`);
};

const memberchurch = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { code } = req.params;

  const [member, memberselect] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    MemberModel.aggregate([
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          state: 1,
          identitynumber: 1,
        },
      },
      {
        $match: { code: code, state: "active" },
      },
      {
        $lookup: {
          from: "memberimage",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { _id: 0, image: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
    ]),
  ]);

  return res.render("member/memberchurch", {
    active: "member",
    tab: "church",
    session: sessioncode,
    member,
    memberselect: memberselect[0],
  });
};

const memberaccess = async (req = request, res = response) => {
  const sessioncode = await session(req);
  const { code } = req.params;

  if (!sessioncode) {
    return res.redirect("/");
  }

  const [member, memberselect, access, level] = await Promise.all([
    MemberModel.findOne(
      {
        code: sessioncode,
        $or: [{ state: "active" }, { state: "inactive" }],
      },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
        identitynumber: 1,
      }
    ).then(
      async ({
        _id,
        code,
        names,
        firstlastname,
        secondlastname,
        state,
        identitynumber,
      }) => {
        const memberaccess = await MemberAccessModel.find(
          { member: _id },
          {
            member: 1,
            access: 1,
            permitcreate: 1,
            permitupdate: 1,
            permitdelete: 1,
          }
        )
          .populate("access", ["access", "accessname"])
          .populate("level", ["level", "levelname"]);

        return {
          _id,
          code,
          names,
          firstlastname,
          secondlastname,
          state,
          identitynumber,
          memberaccess: memberaccess.map((e) => {
            return {
              access: e.access.access,
              accessname: e.access.accessname,
              permitcreate: e.permitcreate,
              permitupdate: e.permitupdate,
              permitdelete: e.permitdelete,
              level: e.level.level,
              levelname: e.level.levelname,
            };
          }),
        };
      }
    ),
    MemberModel.aggregate([
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          state: 1,
          identitynumber: 1,
        },
      },
      {
        $match: { code, state: "active" },
      },
      {
        $lookup: {
          from: "memberimage",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { _id: 0, image: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
    ]),
    AccessModel.find(
      { $or: [{ state: "active" }, { state: "inactive" }] },
      {
        _id: 1,
        access: 1,
        accessname: 1,
        accessnumber: 1,
        state: 1,
      }
    ),
    LevelModel.find(
      { state: "active" },
      {
        _id: 1,
        level: 1,
        levelname: 1,
        levelnumber: 1,
        state: 1,
      }
    ),
  ]);

  const memberaccess = await MemberAccessModel.find({
    member: memberselect[0]._id,
  })
    .populate("access")
    .populate("level");

  let memberaccessselected = [];
  let memberlevelselected = [];
  memberaccess.forEach((e) => {
    memberaccessselected.push(e.access.accessnumber);
    memberlevelselected.push(e.level.level);
  });

  return res.render("member/memberaccess", {
    active: "member",
    tab: "access",
    session: sessioncode,
    member: member,
    memberselect: memberselect[0],
    access: access.sort((a, b) => a.accessnumber - b.accessnumber),
    level: level.sort((a, b) => a.levelnumber - b.levelnumber),
    memberaccess: memberaccess.sort(
      (a, b) => a.access.accessnumber - b.access.accessnumber
    ),
    memberaccesssel: memberaccessselected,
    memberlevelselected,
  });
};

const memberaccesscreate = async (req = request, res = response) => {
  const sessioncode = await session(req);
  const { code } = req.params;
  const { access, create, update, remove, nombre } = req.body;

  console.log(nombre);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const memberselect = await MemberModel.findOne(
    { code, state: "active" },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
      identitynumber: 1,
    }
  );

  const level = await LevelModel.findOne({ levelnumber: 5 });

  try {
    const memberaccessmodel = new MemberAccessModel({
      member: memberselect._id,
      access,
      permitcreate: create === "true" ? true : false,
      permitupdate: update === "true" ? true : false,
      permitdelete: remove === "true" ? true : false,
      level: level._id,
      creationdate: date(),
    });
    await memberaccessmodel.save();
  } catch (error) {
    console.log(error);
  }

  return res.redirect(`/member/${code}/access`);
};

const memberaccessupdate = async (req = request, res = response) => {
  const sessioncode = await session(req);
  const { code, id, permit } = req.params;

  if (!sessioncode) {
    return res.redirect("/");
  }

  try {
    const permiso = await MemberAccessModel.findOne({ _id: id });
    switch (permit) {
      case "create":
        await MemberAccessModel.findOneAndUpdate(
          { _id: id },
          { permitcreate: !permiso.permitcreate }
        );
        break;
      case "update":
        await MemberAccessModel.findOneAndUpdate(
          { _id: id },
          { permitupdate: !permiso.permitupdate }
        );
        break;
      case "delete":
        await MemberAccessModel.findOneAndUpdate(
          { _id: id },
          { permitdelete: !permiso.permitdelete }
        );
        break;
      case "active":
        await MemberAccessModel.findOneAndUpdate(
          { _id: id },
          { state: "inactive" }
        );
        break;
      case "inactive":
        await MemberAccessModel.findOneAndUpdate(
          { _id: id },
          { state: "active" }
        );
        break;
      default:
        await MemberAccessModel.findOneAndUpdate(
          { _id: id },
          { level: permit }
        );
        break;
    }
  } catch (error) {
    return res.redirect(`/member/${code}/access`);
  }

  return res.redirect(`/member/${code}/access`);
};

const memberaccessdelete = async (req = request, res = response) => {
  const sessioncode = await session(req);
  const { code, id } = req.params;

  if (!sessioncode) {
    return res.redirect("/");
  }

  try {
    await MemberAccessModel.findOneAndDelete({ _id: id });
  } catch (error) {
    return res.redirect(`/member/${code}/access`);
  }

  return res.redirect(`/member/${code}/access`);
};

const membersubaccessupdate = async (req = request, res = response) => {
  const sessioncode = await session(req);
  const { code } = req.params;
  const { idlevel } = req.body;

  if (!sessioncode) {
    return res.redirect("/");
  }

  if (idlevel === undefined) {
    return res.redirect(`/member/${code}/access`);
  }

  try {
    const member = await MemberModel.findOne(code);

    if (Array.isArray(idlevel)) {
      console.log("es array");
    } else {
      //const subaccess = await MemberSubAccessModel.findOne({_id:idlevel})

      const subaccess = new MemberSubAccessModel({
        member: member._id,
        level: idlevel,
      });
      subaccess.save();
    }
  } catch (error) {
    return res.redirect(`/member/${code}/access`);
  }

  return res.redirect(`/member/${code}/access`);
};

const membercredential = async (req = request, res = response) => {
  const sessioncode = await session(req);

  const { code } = req.params;

  if (!sessioncode) {
    return res.redirect("/");
  }

  const [member, memberselect] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    MemberModel.aggregate([
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          state: 1,
          identitynumber: 1,
        },
      },
      {
        $match: { code: code, state: "active" },
      },
      {
        $lookup: {
          from: "memberimage",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { _id: 0, image: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
    ]),
  ]);

  console.log(memberselect);

  const imagenres = await imagecredential(code);
  console.log(imagenres);

  return res.render("member/membercredential", {
    active: "member",
    tab: "credential",
    session: sessioncode,
    member,
    memberselect: memberselect[0],
  });
};

const memberdocuments = async (req = request, res = response) => {
  const sessioncode = await session(req);

  const { code } = req.params;

  if (!sessioncode) {
    return res.redirect("/");
  }

  const [member, memberselect] = await Promise.all([
    MemberModel.findOne(
      { code: sessioncode },
      {
        _id: 1,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    MemberModel.aggregate([
      {
        $project: {
          code: 1,
          names: 1,
          firstlastname: 1,
          secondlastname: 1,
          state: 1,
          identitynumber: 1,
        },
      },
      {
        $match: { code: code, state: "active" },
      },
      {
        $lookup: {
          from: "memberimage",
          localField: "_id",
          foreignField: "member",
          as: "images",
          pipeline: [{ $project: { _id: 0, image: 1 } }],
        },
      },
      {
        $unwind: "$images",
      },
    ]),
  ]);

  return res.render("member/memberdocuments", {
    active: "member",
    tab: "documents",
    session: sessioncode,
    member,
    memberselect: memberselect[0],
  });
};

const membercreateadmin = async (req = request, res = response) => {
  const { password } = req.params;

  await data();

  const userpassword = generaterandomnumber(7);

  console.log(userpassword);

  if (password === membercreatekey) {
    const codigo = code();
    const urldata = production ? urlpro : url;

    qrcode(codigo, `${urldata}/${codigo}`);

    const miembro = new MemberModel({
      code: codigo,
      user: userpassword,
      password: userpassword,
      names: randomstring(7),
      firstlastname: randomstring(7),
      secondlastname: randomstring(7),
      identitynumber: userpassword,
      creationdate: date(),
    });
    await miembro.save();

    const memberimage = new MemberImageModel({
      member: miembro._id,
      image: null,
      imagename: null,
      imageextension: null,
      imagewidth: null,
      imageheight: null,
      creationdate: date(),
    });
    await memberimage.save();

    const memberimageqr = new MemberImageCodeQRModel({
      member: miembro._id,
      image: `${codigo}.png`,
      imagename: `${codigo}`,
      imageextension: "png",
      imagewidth: "500",
      imageheight: "500",
      creationdate: date(),
    });
    await memberimageqr.save();

    const memberchurch = new MemberChurchModel({
      member: miembro._id,
      creationdate: date(),
    });
    await memberchurch.save();

    const level = await LevelModel.findOne({ levelnumber: 1 });
    console.log(level);

    const access = await AccessModel.find({ state: "active" });
    access.forEach(async (e) => {
      let memberaccess = new MemberAccessModel({
        member: miembro._id,
        access: e._id,
        level: level._id,
        permitcreate: true,
        permitupdate: true,
        permitdelete: true,
        creationdate: date(),
      });
      await memberaccess.save();
    });

    return res.send(miembro.password);
  } else {
    return res.json({
      message: "Error",
    });
  }
};

module.exports = {
  member,
  memberimageupdate,
  memberupdate,
  memberchurch,
  memberaccess,
  memberaccesscreate,
  memberaccessupdate,
  memberaccessdelete,
  membersubaccessupdate,
  memberdocuments,
  membercredential,
  membercreateadmin,
};
