const { request, response } = require("express");
const MemberModel = require("../models/member");
const DistrictModel = require("../models/district");
const DepartmentModel = require("../models/department");
const CountryModel = require("../models/country");
const session = require("../tools/session");
const date = require("../tools/date");

const district = async (req = request, res = response) => {
  const sessionmember = await session(req);

  if (!sessionmember) {
    return res.redirect("/");
  }

  const [member, districts, departments, countries] = await Promise.all([
    MemberModel.findOne(
      { code: sessionmember },
      {
        _id: 0,
        code: 1,
        names: 1,
        firstlastname: 1,
        secondlastname: 1,
        state: 1,
      }
    ),
    DistrictModel.find(
      {},
      {
        _id: 1,
        district: 1,
      }
    )
      .populate("department", ["_id", "department"]),
    DepartmentModel.find(
      {state: "active"},
      {
        _id: 1,
        department: 1,
        abbreviation: 1,
      }
    ).populate("country", ["_id", "country"]),
    CountryModel.find(
      { $or: [{ state: "active" }, { state: "inactive" }] },
      {
        _id: 1,
        country: 1,
        state: 1,
      }
    ),
  ]);

  console.log(districts);

  return res.render("district/district", {
    active: "district",
    session: sessionmember,
    member:member,
    districts,
    departments,
    countries,
    menu: "district",
  });
};

const districtcreate = async (req = request, res = response) => {
  const sessionmember = await session(req);

  if (!sessionmember) {
    return res.redirect("/");
  }

  const { district, department } = req.body;

  const member = await MemberModel.findOne(
    { code: sessionmember },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const distrito = new DistrictModel({
    district,
    department,
    member: member._id,
    creationdate: date(),
  });
  await distrito.save();

  return res.redirect("/district");
};

const districtcountrycreate = async (req = request, res = response) => {
  const sessionmember = await session(req);

  if (!sessionmember) {
    res.redirect("/");
  }

  const { country } = req.body;

  const member = await MemberModel.findOne(
    { membercode: sessionmember },
    {
      _id: 1,
      membercode: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const pais = new CountryModel({
    country,
    member: member._id,
    creationdate: date(),
  });
  await pais.save();

  res.redirect("/district");
};

const districtdepartmentcreate = async (req = request, res = response) => {
  const sessionmember = await session(req);

  if (!sessionmember) {
    res.redirect("/");
  }

  const { department, abbreviation, country } = req.body;

  const member = await MemberModel.findOne(
    { membercode: sessionmember },
    {
      _id: 1,
      membercode: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const departamento = new DepartmentModel({
    department,
    abbreviation,
    country,
    member: member._id,
    creationdate: date(),
  });
  await departamento.save();

  res.redirect("/district");
};

module.exports = {
  district,
  districtcreate,
  districtcountrycreate,
  districtdepartmentcreate,
};
