const { request, response } = require("express");
const MemberModel = require("../models/member");
const ChurchModel = require("../models/church");
const DistrictModel = require("../models/district");
const session = require("../tools/session");
const date = require("../tools/date");

const churches = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const member = await MemberModel.find(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const churches = await ChurchModel.find({}, { church: 1 }).populate(
    "district",
    ["_id", "district"]
  );
  const districts = await DistrictModel.find({}, { _id: 1, district: 1 });

  return res.render("churches/churches", {
    active: "churches",
    session: sessioncode,
    member: member[0],
    churches,
    districts,
  });
};

const churchescreate = async (req = request, res = response) => {
  const sessioncode = await session(req);

  if (!sessioncode) {
    return res.redirect("/");
  }

  const { church, district } = req.body;

  const member = await MemberModel.findOne(
    { code: sessioncode },
    {
      _id: 1,
      code: 1,
      names: 1,
      firstlastname: 1,
      secondlastname: 1,
      state: 1,
    }
  );

  const iglesia = new ChurchModel({
    church,
    district,
    member: member._id,
    creationdate: date(),
  });
  await iglesia.save();

  return res.redirect("/churches");
};

module.exports = { churches, churchescreate };
