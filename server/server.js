const path = require("path");
const express = require("express");
const cors = require("cors");
const fileupload = require("express-fileupload");
const DBConnection = require("../database/database");
const main = require("../routes/main");
const login = require("../routes/login");
const members = require("../routes/members");
const member = require("../routes/member");
const churches = require("../routes/churches");
const district = require("../routes/district");
const department = require("../routes/department");
const national = require("../routes/national");
const country = require("../routes/country");
const maincontrol = require("../routes/maincontrol");
const information = require("../routes/information");
const reports = require("../routes/reports");
const fonamin = require("../routes/fonamin");
const cookiesession = require("cookie-session");
const {
  port,
  production,
  url,
  urlpro,
} = require("../environments/environment");

const data = require("../data/data");

class Server {
  constructor() {
    this.app = express();
    this.port = port;

    this.path = {
      main: "/",
      login: "/login",
      logout: "/logout",
      members: "/members",
      member: "/member",
      churches: "/churches",
      district: "/district",
      department: "/department",
      country: "/country",
      national: "/national",
      reports: "/reports",
      information: "/information",
      control: "/control",
      fonamin: "/fonamin",
    };

    this.connectionDatabase();
    this.datadatabase();
    this.middlewares();
    this.routes();
  }

  async connectionDatabase() {
    await DBConnection();
  }

  datadatabase() {
    data();
  }

  middlewares() {
    this.app.use(express.static("public"));
    this.app.use(
      fileupload({
        useTempFiles: true,
        tempFileDir: path.join(__dirname, "../tmp"),
        createParentPath: true,
      })
    );
    this.app.set("views", "views");
    this.app.set("view engine", "pug");

    this.app.use(cors());
    this.app.use(express.json({ limit: "100mb" }));
    this.app.use(express.urlencoded({ extended: false, limit: "100mb" }));
    this.app.set("trust proxy", 1);

    this.app.use(
      cookiesession({
        name: "session",
        keys: ["5m282kw8dbbrqw5x7su"],
        maxAge: 1000 * 60 * 60,
      })
    );
  }

  routes() {
    this.app.use(this.path.main, main);
    this.app.use(this.path.login, login);
    this.app.use(this.path.logout, login);
    this.app.use(this.path.members, members);
    this.app.use(this.path.member, member);
    this.app.use(this.path.churches, churches);
    this.app.use(this.path.district, district);
    this.app.use(this.path.department, department);
    this.app.use(this.path.country, country);
    this.app.use(this.path.national, national);
    this.app.use(this.path.reports, reports);
    this.app.use(this.path.information, information);
    this.app.use(this.path.fonamin, fonamin);
    this.app.use(this.path.control, maincontrol);
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log("IEBS Nacional");

      if (production) {
        console.log(`URL: ${urlpro}`);
        console.log(`PORT: ${this.port}`);
      } else {
        console.log(`${url}:${this.port}`);
      }
    });
  }
}

module.exports = Server;
